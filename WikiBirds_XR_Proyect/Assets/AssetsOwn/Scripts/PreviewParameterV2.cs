using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PreviewParameterV2 : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI text;
    string exposedParameter;



    public void setExposedParameter(string newVal)
    {
        if (exposedParameter != newVal)
            text.text = newVal;
        exposedParameter = newVal;
    }
}
