using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableRegulator : MonoBehaviour
{
    [SerializeField] int originalValue;
    [SerializeField] int StepSize;
    private int actualValue;

    [SerializeField] PreviewParameterV2 previewParameter;
    private void Start()
    {
        originalValue = actualValue;
        refreshPreviewParameter();

    }
    public void RaiseTemperature()
    {
        actualValue += StepSize;
        refreshPreviewParameter();
    }

    public void LowerTemperature()
    {
        actualValue -= StepSize;
        refreshPreviewParameter();
    }

    public void ResetTemperature()
    {
        actualValue = originalValue;
        refreshPreviewParameter();
    }

    public int getGlobalTemperature() { return originalValue; }

    private void refreshPreviewParameter()
    {
        if (previewParameter != null)
            previewParameter.setExposedParameter(actualValue.ToString());
    }
}
