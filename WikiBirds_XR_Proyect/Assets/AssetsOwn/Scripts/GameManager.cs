using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    bool cheatsOn = false;
    public void toggleCheats() { cheatsOn = !cheatsOn; }
    public void SetCheats(bool state) { cheatsOn = state; }
    public bool GetCheats() { return cheatsOn; }

    private const int maxPlantas = 100;

    private const int maxTemp = 25;
    private const int minTemp = 10;

    private Luz luzTerraza = Luz.Directa;

    private int potenciaHumidificador = 20; //Pasar al humidificador

    //Cuanto calienta el radiador, si se modifica aqui se modifica en todas
    private int potenciaCalentador = 20;    //Pasar al radiador

    private int humedadTerraza = 10;     //La humedad que har�a en la terraza
    private int minhumedadTerraza = 0;
    private int maxhumedadTerraza = 50;


    private int temperaturaTerraza = 21;     //La temperatura que har�a en la terraza
    private int minTemperaturaTerraza = 10;
    private int maxTemperaturaTerraza = 10;


    GameObject[] plantas;
    int numPlantas = 0;

    //PreviewParameters
    [SerializeField] PreviewParameterV2 previewCalentadorParameter;
    [SerializeField] PreviewParameterV2 previewHumedadParameter;
    [SerializeField] PreviewParameterV2 previewCalentadorParameterExterior;
    [SerializeField] PreviewParameterV2 previewHumedadParameterExterior;

    //Prefabs de plantas(Para encargarlas)
    [SerializeField]
    GameObject[] catalogo;
    [SerializeField]
    Transform[] spawnpoints;
    [SerializeField]
    int maximoPedido;       //Cuantas plantas podemos pedir en un dia
    int encargadasHoy = 0;
    GameObject[] toSpawn; //Las platnas que van a aparecer al dia siguinte

    [SerializeField]
    GameObject libreta;

    public GameObject newInfoCanvas;

    private void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);

        //Esto quizas reviente m�s tarde, esperemos que no
        plantas = new GameObject[maxPlantas];
    }

    private void Start()
    {
        toSpawn = new GameObject[maximoPedido];
        previewCalentadorExterior();
        previewHumidificadorExterior();
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    requestPlanta(0);
        //}
        //if (Input.GetKeyDown(KeyCode.Z))
        //{
        //    finDia();

        //}
    }

    //Luego habr� que instanciar las plantas que toquen, bla bla bla
    public void finDia()
    {
        for (int i = 0; i < numPlantas; i++)
        {
            plantas[i].GetComponent<Planta>().finDelDia();


            plantas[i].GetComponent<Planta>().setTerraza(temperaturaTerraza, humedadTerraza, potenciaCalentador, potenciaHumidificador);
        }

        spawnNewPlants();

        randomizeSet();
        previewCalentadorExterior();
        previewHumidificadorExterior();
    }

    //Randomiza el estado de la terraza
    private void randomizeSet()
    {
        int varaicionTempertarua = Random.Range(1, 6);//Como mucho 5 grados
        int variacionHumedad = Random.Range(1, 11);//Como mucho 10 %

        int aumenta = Random.Range(0, 2);

        if (aumenta == 1)
        {
            if ((temperaturaTerraza + varaicionTempertarua) < maxTemperaturaTerraza)
            {
                temperaturaTerraza += varaicionTempertarua;
                potenciaCalentador += (varaicionTempertarua / 3);
            }
        }
        else
        {
            if ((temperaturaTerraza - varaicionTempertarua) > minTemperaturaTerraza)
            {
                temperaturaTerraza -= varaicionTempertarua;
                potenciaCalentador -= (varaicionTempertarua / 3);
            }
        }

        float aumentaHumedad = Random.Range(0, 2);

        if (aumentaHumedad == 1)
        {
            if ((humedadTerraza + variacionHumedad) < maxhumedadTerraza)
            {
                humedadTerraza += variacionHumedad;
                potenciaHumidificador += (variacionHumedad / 3);
            }
        }
        else
        {
            if ((humedadTerraza - variacionHumedad) > minhumedadTerraza)
            {
                humedadTerraza -= variacionHumedad;
                potenciaHumidificador -= (variacionHumedad / 3);
            }
        }


    }

    public Luz luzTerr() { return luzTerraza; }

    public int humedad() { return humedadTerraza; }

    public int temperatura() { return temperaturaTerraza; }

    public int rad() { return potenciaCalentador; }

    public int hum() { return potenciaHumidificador; }

    public void setPlanta(GameObject p)
    {
        plantas[numPlantas] = p;
        numPlantas++;
    }

    public void requestPlanta(int tipo)
    {
        if (encargadasHoy >= maximoPedido)
        {
            //Mensajito de que no se pueden pedir m�s
        }
        else if (tipo < catalogo.Length)
        {
            toSpawn[encargadasHoy] = catalogo[tipo];
            encargadasHoy++;
        }
    }

    private void spawnNewPlants()
    {
        for (int i = 0; i < encargadasHoy; i++)
        {
            Instantiate(toSpawn[i], spawnpoints[i].position, Quaternion.identity);
        }

        encargadasHoy = 0;

        toSpawn = new GameObject[maximoPedido];//Para limpiar el pedido del dia
    }

    //Calentador Methods
    public void setCalentador(int c) { potenciaCalentador = c; }
    public void addToCalentador(int c) { potenciaCalentador += c; }
    public void quitToCalentador(int c) { potenciaCalentador -= c; }
    public void previewCalentador() { previewCalentadorParameter.setExposedParameter(potenciaCalentador.ToString()); }

    //Humedad Methods
    public void setHumidificador(int c) { potenciaHumidificador = c; }
    public void addToHumidificador(int c) { potenciaHumidificador += c; }
    public void quitToHumidificador(int c) { potenciaHumidificador -= c; }
    public void previewHumidificador() { previewHumedadParameter.setExposedParameter(potenciaHumidificador.ToString()); }
    //Preview OutsideParameters
    public void previewHumidificadorExterior() { previewHumedadParameterExterior.setExposedParameter(humedadTerraza.ToString()); }
    public void previewCalentadorExterior() { previewCalentadorParameterExterior.setExposedParameter(temperaturaTerraza.ToString()); }


    public GameObject getLibreta() { return libreta; }

}
