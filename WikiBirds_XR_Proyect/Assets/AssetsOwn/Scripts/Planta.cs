using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

/// <summary>
/// Reseteamos humedad y temperatura cuando pasa el dia o lo dejamos igual.
/// Cuanta agua pierden las plantas al pasar el dia
/// Mirar lo de la poda
/// Que diferencias queremos que sean criticas
/// </summary>


//Tipos de plantas que tenemos en el juego (A�adir aqui si se quieren meter mas)
public enum tipo { Monstera, Violeta, Peyote, Corona, Anturio, Aspi, Tomatera, Jade };
public enum Plagas { Ninguna, Aranias, Moscas, Gusanos };
public enum Luz { Directa, Indirecta, Penumbra };

public enum edad { pequenia, mediana, grande, muerta };


public enum estado
{
    ninguno, muySeca, algoSeca, pocoSeca, pocoAgua, algoAgua, muchoAgua,
    muyPocaTem, algoPocaTem, pocoTem, pocoCal, algoCal, muchoCal,
    muyPocoHum, algoPocoHum, pocoHum, pasasHum, algoHum, muyHum,
    noLuz, Sombra
}

public class Planta : MonoBehaviour
{
    GameManager gM;

    Luz luzTerraza;

    int potenciaHumidificador;

    //Cuanto calienta el radiador, si se modifica aqui se modifica en todas
    int potenciaCalentador;

    int humedadTerraza;     //La humedad que har�a en la terraza

    int temperaturaTerraza;     //La temperatura que har�a en la terraza

    //Representa la planta que es
    public tipo tipo_;

    public Plagas plaga = Plagas.Ninguna;

    //Luz actual
    public Luz tipoDeLuzRecibidaPorShandokan = Luz.Directa;

    //Edad de la planta
    edad edadActual;
    [SerializeField] bool randomEdad;

    //Tanto la retencion como la resistencia se representan como porcentaje

    //Representa como de dificil es para la planta PERDER agua
    public float retencion;

    //Representa como de dificil es para la planta COGER agua de la maceta
    public int resistencia;

    //Nivel 1,2 y 3 de como de mal esta la planta respecto a sus rangos ideales. (Pueden ser negativas)
    public int[] diferenciasHumedad;
    public int[] diferenciasAgua;
    public int[] diferenciasTemperatura;
    public int[] diferenciasLuz;

    //A que temperatura esta ahora mismpo la planta
    private int temperatura_Actual;        //En grados centigrados
    public int get_temperatura_Actual() { return temperatura_Actual; }

    //Como de humedad esta ahora mismo la planta
    private int humedad_Actual;                 //Puede ser que estas inicializaciones haya que cambiarlas m�s tarde (Son los valores con los que empiezan las plantas)
    public int get_humedad_Actual() { return humedad_Actual; }

    //Cuanta agua tiene la planta ahora mismo
    private int agua_Actual = 0;

    //El agua que tiene ahora mismo la maceta, no lo hago en otro script, porque lo unico que tendr�a ser�a esta variable, y 
    //Como no vamos a poder tener plantas sin maceta, es irrelevante,
    private int aguaEnTierra = 0;
    public int get_aguaEnTierra() { return aguaEnTierra; }

    //La humedad que le a�adimos con el flusflus
    private int humedadAcumulada = 0;

    //Los rangos OPTIMOS de la planta en estos tres aspectos (Son arrays de dos porque C# no tiene pair)
    //  ***** IMPORTATE : El elemento 0 es el MINIMO y el elemento 1 es el M�XIMO *****
    //SOLO SON POSITIVAS
    public int[] rangoTemperatura = new int[2];
    public int[] rangoHumedad = new int[2];
    public int[] rangoAgua = new int[2];

    public Luz perfectLight;

    //Ser� el objeto de la escena 
    GameObject calentador;

    GameObject humidificador;

    //Para que sepamos si el calentador/humificador tienen que funcionar o no
    bool enInvernadero = false;

    //empieza sana
    private int salud_Agua = 0;
    private int salud_Temperatura = 0;
    private int salud_Humedad = 0;
    private int salud_Luz = 0;


    //Componentes para graficos
    MeshRenderer render;

    //Materiales para los cambios de estado
    [SerializeField]
    Material materialTierraOriginal;
    [SerializeField] GameObject tierraMaceta;
    Material materialTierra;
    [SerializeField]
    Material materialAguaOriginal;
    [SerializeField] GameObject aguaMaceta;
    Material materialAguaMaceta;
    [SerializeField] GameObject aguaPlato;
    Material materialAguaPlato;

    //Prefabs para los cambios de modelo
    [SerializeField]
    GameObject pequenia;
    [SerializeField]
    GameObject mediana;
    [SerializeField]
    GameObject grande;
    [SerializeField]
    GameObject pequeniaMuerta;
    [SerializeField]
    GameObject medianaMuerta;
    [SerializeField]
    GameObject grandeMuerta;

    //Colores 
    [SerializeField]
    Color verde;
    [SerializeField]
    Color colorSeco;
    [SerializeField]
    Color colorMuySeco;
    [SerializeField]
    Color colorPodrido;
    [SerializeField]
    Color colorMuyPodrido;
    [SerializeField]
    Color colorMuerto;

    //Si est�n todas a true la planta crece tras X dias
    bool[] condiciones = new bool[4];


    //PreviewParameters
    [SerializeField] PreviewParameterV2 cantidadAguaParameter;
    [SerializeField] PreviewParameterV2 cantidadHumedadParameter;
    [SerializeField] PreviewParameterV2 cantidadTemperaturaParameter;
    [SerializeField] PreviewParameterV2 cantidadPlagaParameter;
    [SerializeField] PreviewParameterV2 cantidadLuzParameter;

    //Para hacerla crecer
    public int daysToGrow;
    int perfectDays = 0;

    //Para que se muera
    bool sickAux = false; //Si ya hemos contado uno de los atributos de enfermedad como nefasto ese dia
    public int daysToDie;
    int sickDays = 0;

    //Lo usamos para mostrar el estado de la planta, pque�a, mediana etc
    [SerializeField]
    Transform basePos;
    GameObject plantStage;


    //Para curar una planta
    [SerializeField]
    int daysToHeal; //El número de dias que necesita ser rociada para curarse
    int daysSprayed = 0;

    //Sanar plaga
    private bool healedInDay;

    private void Update()
    {

        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    render.material.SetColor("_Color", colorMuerto);
        //}

        //Esto es para debugear
        if (Input.GetKeyDown("space"))
        {
            render.material.SetColor("_Color", colorMuySeco);
        }


        //Esto es para debugear
        if (Input.GetKeyDown(KeyCode.R))
        {
            //temperatura_Actual = -50;
        }

        //Esto es para debugear
        if (Input.GetKeyDown(KeyCode.T))
        {
            agua_Actual = rangoAgua[1] - 5;
            humedad_Actual = rangoHumedad[1] - 5;
            temperatura_Actual = 17;
        }

        //Esto es para debugear
        if (Input.GetKeyDown(KeyCode.Z))
        {
            finDelDia();
        }

        //Esto es para debugear
        if (Input.GetKeyDown(KeyCode.M))
        {
            antiPlagas(2);
        }

        //Esto es para debugear
        if (Input.GetKeyDown(KeyCode.J))
        {
            regada(5);
        }
    }

    private void Start()
    {
        gM = GameManager.instance;


        calentador = GameObject.FindGameObjectWithTag("Calentador");
        humidificador = GameObject.FindGameObjectWithTag("Humificador");

        temperaturaTerraza = gM.temperatura();
        humedadTerraza = gM.humedad();
        luzTerraza = gM.luzTerr();
        potenciaCalentador = gM.rad();
        potenciaHumidificador = gM.hum();

        temperatura_Actual = temperaturaTerraza;
        humedad_Actual = humedadTerraza;

        gM.setPlanta(gameObject);

        previewAll();

        // Tierra
        materialTierra = new Material(materialTierraOriginal);
        materialTierra.CopyPropertiesFromMaterial(materialTierraOriginal);
        materialTierra.shader = materialTierraOriginal.shader;
        tierraMaceta.GetComponent<MeshRenderer>().material = materialTierra;

        // Agua Maceta
        materialAguaMaceta = new Material(materialAguaOriginal);
        materialAguaMaceta.CopyPropertiesFromMaterial(materialAguaOriginal);
        materialAguaMaceta.shader = materialAguaOriginal.shader;
        aguaMaceta.GetComponent<MeshRenderer>().material = materialAguaMaceta;

        // Agua Plato
        materialAguaPlato = new Material(materialAguaOriginal);
        materialAguaPlato.CopyPropertiesFromMaterial(materialAguaOriginal);
        materialAguaPlato.shader = materialAguaOriginal.shader;
        aguaPlato.GetComponent<MeshRenderer>().material = materialAguaPlato;

        if (randomEdad)
        {
            int ramdomInt = UnityEngine.Random.Range(0, 2);
            switch (ramdomInt)
            {
                case 0:
                    edadActual = edad.mediana;
                    plantStage = Instantiate(mediana, basePos);
                    render = plantStage.GetComponent<MeshRenderer>();

                    // ESTADO MATERIAL, activamos el que queremos, desactivamos el resto
                    render.material.DisableKeyword("ESTADO_PEQUENA");
                    render.material.EnableKeyword("ESTADO_MEDIANA");
                    render.material.DisableKeyword("ESTADO_GRANDE");
                    render.material.DisableKeyword("ESTADO_MUERTA");
                    break;
                case 1:
                    edadActual = edad.grande;
                    plantStage = Instantiate(grande, basePos);
                    render = plantStage.GetComponent<MeshRenderer>();

                    render.material.DisableKeyword("ESTADO_PEQUENA");
                    render.material.DisableKeyword("ESTADO_MEDIANA");
                    render.material.EnableKeyword("ESTADO_GRANDE");
                    render.material.DisableKeyword("ESTADO_MUERTA");
                    break;
            }
        }
        else
        {
            edadActual = edad.pequenia;
            plantStage = Instantiate(pequenia, basePos);
            render = plantStage.GetComponent<MeshRenderer>();

            // ESTADO MATERIAL, activamos el que queremos, desactivamos el resto
            render.material.EnableKeyword("ESTADO_PEQUENA");
            render.material.DisableKeyword("ESTADO_MEDIANA");
            render.material.DisableKeyword("ESTADO_GRANDE");
            render.material.DisableKeyword("ESTADO_MUERTA");
        }

    }

    //Para el humidificador
    private void CalculaHumedad()
    {
        if (enInvernadero)
        {
            Vector3 DistHum = transform.position - humidificador.transform.position;
            humedad_Actual = humedadTerraza + (potenciaHumidificador - (int)DistHum.magnitude);
        }
    }

    //Para el radiador
    private void CalculaTemperatura()
    {
        //Si esta en el invernadero le subimos la temperatura en funcion a como de cerca esta del calentador (MAXIMA = temperatura_Actual, ahora mismo 20, + potenciaCalentador)
        if (enInvernadero)
        {
            //Como de lejos estamos del radiador
            Vector3 DistRad = transform.position - calentador.transform.position;
            temperatura_Actual = temperaturaTerraza + (potenciaCalentador - (int)DistRad.magnitude);
        }
        else
        {

            temperatura_Actual = gM.temperatura();
        }
    }

    private void CalculaLuz()
    {
        if (tipoDeLuzRecibidaPorShandokan != perfectLight)
        {
            if (tipoDeLuzRecibidaPorShandokan == Luz.Penumbra) salud_Luz += 10;
            else salud_Luz += 5;
        }
    }

    //Si esta dentro del invernadero y sale, lo saca, y viceversa.
    public void invernadero()
    {
        enInvernadero = !enInvernadero;

        //Si sale del invernadero, lo ponemos a la humedad y temperatura de la terraza.
        if (!enInvernadero)
        {
            resetHumedad();
            resetTemperatura();
        }
    }

    //A este m�todo hay que llamarlo cuando impacten las p�rticulas de agua
    //A�adir lo que se quita por la resistencia
    public void regada(int cantidad)
    {
        aguaEnTierra += cantidad;
        cambioMaterialTierra(aguaEnTierra);
    }

    void cambioMaterialTierra(int aguaEnTierra)
    {
        // Material tierra // se puede optimizar pero me la pela (de momento)
        int max_humedad = 70;
        int min_humedad = 0;
        int max_agua = 90;
        int max_plato = 100;

        float humedad = normalizar(min_humedad, max_humedad, aguaEnTierra);
        if (aguaEnTierra > max_humedad)
            humedad = 1;

        float aguaMaceta = normalizar(max_humedad, max_agua, aguaEnTierra);
        if (aguaEnTierra >= max_agua)
            aguaMaceta = 1;

        float aguaPlato = normalizar(max_agua, max_plato, aguaEnTierra);
        if (aguaEnTierra >= max_plato)
            aguaPlato = 1;


        materialTierra.SetFloat("_Agua", humedad);
        materialAguaMaceta.SetFloat("_altura", aguaMaceta);
        materialAguaMaceta.SetFloat("_alpha", aguaMaceta);
        materialAguaPlato.SetFloat("_altura", aguaPlato);
        materialAguaPlato.SetFloat("_alpha", aguaPlato);
    }

    // te devuelve un valor entre 0-1
    float normalizar(float min, float max, float x)
    {
        return (x-min) / (max-min);
    }

    public void setRadiador(int cantidad) { potenciaCalentador = cantidad; }

    public void calculoAgua()
    {
        //El agua que la planta se resiste a coger
        int agua_NoAbsorbida = (aguaEnTierra * resistencia) / 100;

        //El agua que absorbe la planta
        int agua_Absorbida = aguaEnTierra - agua_NoAbsorbida;

        //Le sumamos el agua que absorbe
        agua_Actual += agua_Absorbida;

        //El porcentaje de agua que se queda
        int agua_Perdida = (int)((1.0f / retencion) * 100);

        agua_Actual = agua_Actual - agua_Perdida;

        //Le restamos a las tierra el agua que abosorbe la planta
        Debug.Log(aguaEnTierra);
        aguaEnTierra -= agua_Absorbida;

        // Material tierra
        cambioMaterialTierra(aguaEnTierra);
        Debug.Log(aguaEnTierra);
    }

    public void setLuz(Luz nL) { tipoDeLuzRecibidaPorShandokan = nL; }

    //Llamarlo cuando impacten las particulas del flusflus
    public void flusflus(int cantidad)
    {
        humedad_Actual += cantidad;
        humedadAcumulada += cantidad;
    }

    void resetTemperatura() { temperatura_Actual = temperaturaTerraza; }
    void resetHumedad() { humedad_Actual = humedadTerraza + humedadAcumulada; }

    public void finDelDia()
    {
        //Comprobar los valores actuales con los rangos ideales

        CalculaHumedad();

        CalculaLuz();

        CalculaTemperatura();

        calculoAgua();
        //Comprobamos los estados en funcion de los valores antes calculador
        estadoHumedad();

        estadoLuz();

        estadoTemperatura();

        estadoAgua();

        checkHealing();

        //Probabilidad de que haya una plaga conforme a su salud
        plagueProb();

        previewAll();

        checkGrowth();

        checkSickness();

        probHojas();

        checkSprayPlagas();

        checkNotas();
    }

    private void checkNotas()
    {
        GetComponent<notaSpawner>().finDeDia();
    }

    private void probHojas()
    {
        GetComponent<Pochas>().finDia();
    }

    private void checkHealing()
    {
        if (daysSprayed >= daysToHeal)
        {
            plaga = Plagas.Ninguna;
            render.material.SetInt("_Plaga", 0);
            daysSprayed = 0;
        }
    }

    private void plagueProb()
    {
        //To DO: Chekear para que sea mas acurate
        float prob = salud_Agua + salud_Humedad + salud_Luz + salud_Temperatura;

        float random = UnityEngine.Random.Range(0, 101);

        if (random <= prob || -random >= prob)
        {

            render.material.SetInt("_Plaga", 1);
            if (salud_Agua > salud_Humedad && salud_Agua > salud_Luz && salud_Agua > salud_Temperatura) //Si esta mal de agua
            {

                plaga = Plagas.Gusanos;
                render.material.EnableKeyword("PLAGA_BLANCA");
                render.material.DisableKeyword("PLAGA_ROJA");
                render.material.DisableKeyword("PLAGA_AMARILLA");
            }
            else if (salud_Humedad > salud_Agua && salud_Humedad > salud_Luz && salud_Humedad > salud_Temperatura)   // Si esta mal de humedad
            {

                plaga = Plagas.Aranias;
                render.material.EnableKeyword("PLAGA_ROJA");
                render.material.DisableKeyword("PLAGA_BLANCA");
                render.material.DisableKeyword("PLAGA_AMARILLA");
            }
            else   //Si esta mal de luz o de temperatura
            {

                plaga = Plagas.Moscas;
                render.material.EnableKeyword("PLAGA_AMARILLA");
                render.material.DisableKeyword("PLAGA_ROJA");
                render.material.DisableKeyword("PLAGA_BLANCA");
            }

        }
    }

    private void checkSickness()
    {
        if (plaga != Plagas.Ninguna && !sickAux)
        {
            sickDays++;
            sickAux = true;
        }

        if (sickDays > 0) perfectDays = 0;

        if (sickDays == daysToDie)
        {
            Destroy(plantStage);
            if (edadActual == edad.grande)
                plantStage = Instantiate(grandeMuerta, basePos);
            else if (edadActual == edad.mediana)
                plantStage = Instantiate(grandeMuerta, basePos);
            else
                plantStage = Instantiate(pequeniaMuerta, basePos);

            render = plantStage.GetComponent<MeshRenderer>();
            render.material.SetColor("_Color", colorMuerto);

            //Para que funcione el enum hay que activar el bueno y desactivar el resto
            render.material.EnableKeyword("ESTADO_MUERTA");
            render.material.DisableKeyword("ESTADO_PEQUENA");
            render.material.DisableKeyword("ESTADO_MEDIANA");
            render.material.DisableKeyword("ESTADO_GRANDE");

        }
        sickAux = false;
    }

    private void checkGrowth()
    {
        int i = 0;
        while (i < condiciones.Length && condiciones[i]) i++;

        if (i == condiciones.Length)
        {
            perfectDays++;
            render.material.SetColor("_Color", verde);
            sickDays = 0;
        }

        if (perfectDays == daysToGrow && edadActual != edad.muerta)
        {
            //Crece
            Destroy(plantStage);

            if (edadActual == edad.pequenia)
            {
                plantStage = Instantiate(mediana, basePos);
                render.material.EnableKeyword("ESTADO_MEDIANA");
                render.material.DisableKeyword("ESTADO_PEQUENA");
                render.material.DisableKeyword("ESTADO_GRANDE");
                render.material.DisableKeyword("ESTADO_MUERTA");
            }
            else if (edadActual == edad.mediana)
            {
                plantStage = Instantiate(grande, basePos);
                render.material.EnableKeyword("ESTADO_GRANDE");
                render.material.DisableKeyword("ESTADO_PEQUENA");
                render.material.DisableKeyword("ESTADO_MEDIANA");
                render.material.DisableKeyword("ESTADO_MUERTA");
            }

            render = plantStage.GetComponent<MeshRenderer>();

            edadActual++;
            perfectDays = 0;
        }
    }

    private void checkSprayPlagas()
    {
        if (healedInDay)
            daysSprayed++;
        else
            daysSprayed = 0;

        healedInDay = false;
    }

    public void setTerraza(int temperatura, int humedad, int rad, int hum)
    {
        temperaturaTerraza = temperatura;
        humedadTerraza = humedad;
        potenciaCalentador = rad;
        potenciaHumidificador = hum;
    }

    public void antiPlagas(int tipo)
    {
        if (tipo == (int)plaga)
        {
            if (!healedInDay)
                healedInDay = true;
        }
    }

    //C�lculos de fin del d�a
    private void estadoAgua()
    {
        if (agua_Actual <= rangoAgua[1] && agua_Actual >= rangoAgua[0])
        {
            //Tamos gucci, la planta crece
            condiciones[3] = true;

            calculateHealth(ref salud_Agua);
        }
        else
        {
            condiciones[3] = false;
            //Calculamos la difrencia del dia actual
            int diferencia = 0;

            //SI es mayor que la maxima
            if (agua_Actual > rangoAgua[1]) diferencia = agua_Actual - rangoAgua[1];
            else diferencia = agua_Actual - rangoAgua[0]; //Es meor que el minimo

            //Se la a�adimos a la diferencia general
            salud_Agua += diferencia;

            //Comprobamos el estado en funcion de la diferencia
            int estado = calculaDiferencia(salud_Agua, diferenciasAgua);

            switch (estado)
            {
                //Muy seca
                case -3:
                    render.material.SetColor("_Color", colorMuySeco);

                    break;
                // Bastante seca
                case -2:
                    render.material.SetColor("_Color", colorSeco);

                    break;
                //Un poco seca
                case -1:
                    break;
                //Te pasas un poco
                case 1:

                    break;
                //Te pasas bastate
                case 2:
                    render.material.SetColor("_Color", colorPodrido);

                    break;
                //Te pasas mucho
                case 3:
                    render.material.SetColor("_Color", colorMuyPodrido);

                    break;

                default:
                    break;
            }
        }
    }

    private void calculateHealth(ref int h)
    {
        if (h <= 2 && h >= -2) h = 0;
        else if (h < 0) h = h + (-h / 2);
        else if (h > 0) h = h - (h / 2);

    }

    private void estadoTemperatura()
    {
        if (temperatura_Actual <= rangoTemperatura[1] && temperatura_Actual >= rangoTemperatura[0])
        {
            //Tamos gucci, la planta crece
            condiciones[2] = true;

            calculateHealth(ref salud_Temperatura);
        }
        else
        {
            condiciones[2] = false;
            int diferencia = 0;

            if (temperatura_Actual > rangoTemperatura[1]) diferencia = temperatura_Actual - rangoTemperatura[1];
            else diferencia = temperatura_Actual - rangoTemperatura[0]; //Es meor que el minimo

            salud_Temperatura += diferencia;

            int estado = calculaDiferencia(salud_Temperatura, diferenciasTemperatura);

            switch (estado)
            {
                //Muy fria
                case -3:
                    render.material.SetColor("_Color", colorMuyPodrido);
                    break;
                // Bastante fria
                case -2:
                    render.material.SetColor("_Color", colorPodrido);
                    break;
                //Un poco fria
                case -1:

                    break;
                //Te pasas un poco
                case 1:
                    break;
                //Te pasas bastate
                case 2:
                    render.material.SetColor("_Color", colorSeco);
                    //Te secas que te cagas
                    //Material = seca
                    break;
                //Te pasas mucho
                case 3:
                    render.material.SetColor("_Color", colorMuySeco);

                    break;

                default:
                    break;
            }
        }
    }

    private void estadoHumedad()
    {
        if (humedad_Actual <= rangoHumedad[1] && humedad_Actual >= rangoHumedad[0])
        {
            condiciones[1] = true;
            calculateHealth(ref salud_Humedad);
        }
        else
        {
            condiciones[1] = false;
            int diferencia = humedad_Actual - rangoHumedad[1];

            if (humedad_Actual > rangoHumedad[1]) diferencia = humedad_Actual - rangoHumedad[1];
            else diferencia = humedad_Actual - rangoHumedad[0]; //Es meor que el minimo

            salud_Humedad += diferencia;

            int estado = calculaDiferencia(salud_Humedad, diferenciasHumedad);

            switch (estado)
            {
                //Muy seca
                case -3:
                    render.material.SetColor("_Color", colorMuySeco);

                    break;
                // Bastante seca
                case -2:
                    render.material.SetColor("_Color", colorSeco);
                    break;
                //Un poco seca
                case -1:
                    break;
                //Te pasas un poco
                case 1:
                    break;
                //Te pasas bastate
                case 2:
                    render.material.SetColor("_Color", colorPodrido);
                    break;
                //Te pasas mucho
                case 3:
                    render.material.SetColor("_Color", colorMuyPodrido);
                    break;

                default:
                    break;
            }
        }
    }

    private void estadoLuz()
    {
        if (tipoDeLuzRecibidaPorShandokan == perfectLight)
        {
            condiciones[0] = true;
            calculateHealth(ref salud_Luz);
        }
        else
        {
            condiciones[0] = false;

            int estado = calculaDiferencia(salud_Luz, diferenciasLuz);

            switch (estado)
            {
                //Muy seca
                case -3:
                    render.material.SetColor("_Color", colorMuyPodrido);
                    break;
                // Bastante seca
                case -2:
                    render.material.SetColor("_Color", colorPodrido);
                    break;
                //Un poco seca
                case -1:
                    break;
                //Te pasas un poco
                case 1:
                    break;
                //Te pasas bastate
                case 2:
                    render.material.SetColor("_Color", colorSeco);
                    break;
                //Te pasas mucho
                case 3:
                    render.material.SetColor("_Color", colorMuySeco);
                    break;

                default:
                    break;
            }
        }
    }

    //Te devuelve en un n�mero el estado de la planta en el atributo que quieras
    private int calculaDiferencia(int diferencia, int[] diferencias)
    {
        if (diferencia >= diferencias[2])
        {
            if (!sickAux)
            {
                sickDays++;
                sickAux = true;

            }
            return 3;
            //Mega h�meda
        }
        else if (diferencia >= diferencias[1])
        {
            return 2;
            //Bastante h�meda
        }
        else if (diferencia >= diferencias[0])
        {
            return 1;
            //Se pasa un poquito
        }//La diferencia es negativa
        else if (diferencia <= -diferencias[2])
        {
            if (!sickAux)
            {
                sickDays++;
                sickAux = true;

            }
            return -3;
            //Mega seca
        }
        else if (diferencia <= -diferencias[1])
        {
            return -2;
            //Bastante seca
        }
        else //No quedan mas opciones
        {
            return -1;
            //Un poquito seca
        }
    }

    //Metodo para cuando cambian las condiciones de la terraza

    public void previewAguaActual()
    {
        if (cantidadAguaParameter != null)
            cantidadAguaParameter.setExposedParameter(agua_Actual.ToString());
    }
    public void previewHumedadActual()
    {
        if (cantidadHumedadParameter != null)
            cantidadHumedadParameter.setExposedParameter(humedad_Actual.ToString());
    }
    public void previewTemperaturaActual()
    {
        if (cantidadTemperaturaParameter != null)
            cantidadTemperaturaParameter.setExposedParameter(temperatura_Actual.ToString());
    }
    public void previewPlagaActual()
    {
        if (cantidadPlagaParameter != null)
            cantidadPlagaParameter.setExposedParameter(plaga.ToString());
    }

    public void previewCantidadLuz()
    {
        if (cantidadLuzParameter != null)
            cantidadLuzParameter.setExposedParameter(luzTerraza.ToString());
    }

    public void previewAll()
    {
        previewAguaActual();
        previewHumedadActual();
        previewTemperaturaActual();
        previewPlagaActual();
        previewCantidadLuz();
    }

    public estado notas()
    {

        int i = 3;

        while (i > 0 && condiciones[i]) i--;


        int dif = 0;

        if (i == 3)
        {
            dif = calculaDiferencia(salud_Agua, diferenciasAgua);

            if (dif == -3) return estado.muySeca;
            else if (dif == -2) return estado.muySeca;
            else if (dif == -1) return estado.pocoSeca;
            else if (dif == 1) return estado.pocoAgua;
            else if (dif == 2) return estado.algoAgua;
            else return estado.muchoAgua;
        }
        if (i == 2)
        {
            dif = calculaDiferencia(salud_Temperatura, diferenciasTemperatura);

            if (dif == -3) return estado.muyPocaTem;
            else if (dif == -2) return estado.algoPocaTem;
            else if (dif == -1) return estado.pocoTem;
            else if (dif == 1) return estado.pocoCal;
            else if (dif == 2) return estado.algoCal;
            else return estado.muchoCal;
        }
        if (i == 1)
        {
            dif = calculaDiferencia(salud_Humedad, diferenciasHumedad);

            if (dif == -3) return estado.muyPocoHum;
            else if (dif == -2) return estado.algoPocoHum;
            else if (dif == -1) return estado.pocoHum;
            else if (dif == 1) return estado.pasasHum;
            else if (dif == 2) return estado.algoHum;
            else return estado.muyHum;
        }
        if (i == 0)
        {
            if (tipoDeLuzRecibidaPorShandokan != perfectLight)
            {
                if (tipoDeLuzRecibidaPorShandokan == Luz.Penumbra) return estado.Sombra;
                else return estado.noLuz;
            }
        }

        return estado.ninguno;
    }

    public tipo getTipo() { return tipo_; }

    public Plagas GetPlagas() { return plaga; }
}
