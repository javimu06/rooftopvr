using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectAnim : MonoBehaviour
{
    // This scripts contains all animations actions for all objects to be called by the XR Interactable Events

    [SerializeField]
    private Animator objectAnimator;
    [SerializeField]
    private string StateAnimationName;
    bool animationDirection = false;

    // Start is called before the first frame update
    void Start()
    {
        if (objectAnimator == null)
            objectAnimator = GetComponent<Animator>();
        if (StateAnimationName.Length == 0)
            StateAnimationName = "";
    }

    //Pull the Trigger
    public void PlayAnimation()
    {
        objectAnimator.Play(StateAnimationName, -1, 0f);
        animationDirection = false;
    }

    public void PlayAnimationReversed()
    {
        objectAnimator.Play("Reversed" + StateAnimationName);
        animationDirection = true;
    }

    public void switchAnimation()
    {
        if (animationDirection)
            PlayAnimation();
        else
            PlayAnimationReversed();
    }
}
