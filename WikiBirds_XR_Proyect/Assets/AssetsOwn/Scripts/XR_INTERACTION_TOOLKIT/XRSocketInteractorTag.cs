using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class XRSocketInteractorTag : XRSocketInteractor
{
    public List<string> targetTags;
    private IXRSelectInteractable lastInteractableHolded;


    protected override bool ShouldDrawHoverMesh(MeshFilter meshFilter, Renderer meshRenderer, Camera mainCamera)
    {
        bool aux = false;
        foreach (string auxString in targetTags)
        {
            if (meshFilter.gameObject.tag == auxString)
                aux = true;
        }

        return base.ShouldDrawHoverMesh(meshFilter, meshRenderer, mainCamera) && aux;
    }

    public override bool CanSelect(IXRSelectInteractable interactable)
    {
        bool aux = false;
        foreach (string auxString in targetTags)
        {
            if (interactable.transform.tag == auxString)
                aux = true;
        }

        return base.CanSelect(interactable) && aux;
    }

    protected override void OnSelectEntered(SelectEnterEventArgs args)
    {
        base.OnSelectEntered(args);

        if (this.gameObject.activeSelf == true && this.GetOldestInteractableSelected() != null)
        {
            lastInteractableHolded = this.GetOldestInteractableSelected();
            //lastInteractableHolded.transform.SetParent(this.gameObject.transform);
            //lastInteractableHolded.transform.localScale = lastInteractableHolded.transform.localScale * lastInteractableHolded.transform.parent.localScale.x;
        }
    }

    protected override void OnSelectExited(SelectExitEventArgs args)
    {
        base.OnSelectExited(args);

        if (this.gameObject.activeSelf == true && this.GetOldestInteractableSelected() != null)
        {
            lastInteractableHolded = this.GetOldestInteractableSelected();
            //float escala = lastInteractableHolded.transform.parent.localScale.x;
            lastInteractableHolded.transform.SetParent(null);
            //lastInteractableHolded.transform.localScale = lastInteractableHolded.transform.localScale / escala;

        }
    }
}
