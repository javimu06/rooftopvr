using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class ColorChanger : MonoBehaviour
{
    public Material selectMaterial = null;

    private MeshRenderer meshRenderer = null;
    private XRBaseInteractable interactable = null;
    private Material originalMaterial = null;

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        originalMaterial = meshRenderer.material;

        interactable = GetComponent<XRBaseInteractable>();
        interactable.hoverEntered.AddListener(SetSelectMaterial);
        interactable.hoverExited.AddListener(SetOriginalMaterial);
    }

    private void OnDestroy()
    {
        interactable.hoverEntered.RemoveListener(SetSelectMaterial);
        interactable.hoverExited.RemoveListener(SetOriginalMaterial);
    }

    public void SetSelectMaterial(HoverEnterEventArgs interactor)
    {
        meshRenderer.material = selectMaterial;
    }
    public void SetOriginalMaterial(HoverExitEventArgs interactor)
    {
        meshRenderer.material = originalMaterial;
    }
}
