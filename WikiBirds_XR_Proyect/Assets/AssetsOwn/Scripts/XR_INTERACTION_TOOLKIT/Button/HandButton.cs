using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

public class HandButton : XRBaseInteractable
{
    private float previousHandHeight = 0.0f;
    private XRBaseInteractor interactor = null;

    private float yMin = 0.0f;
    private float yMax = 0.0f;

    public UnityEvent OnPress = null;
    private bool previousPress = false;

    public float timeToPress;
    private float actualTime;

    protected override void Awake()
    {
        base.Awake();
        hoverEntered.AddListener(StartPress);
        hoverExited.AddListener(EndPress);
    }

    public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
    {
        if (interactor)
        {
            //Calculate last position diference
            float newHandHeight = GetLocalYPosition(interactor.transform.position);
            float handDifference = previousHandHeight - newHandHeight;
            previousHandHeight = newHandHeight;

            float newPosition = transform.localPosition.y - handDifference;
            SetYPosition(newPosition);


            CheckPress();

        }
    }

    private void Start()
    {
        SetMinMax();
    }
    private void Update()
    {
        actualTime += Time.deltaTime;
    }

    private void SetMinMax()
    {
        Collider collider = GetComponent<Collider>();
        //Moving Button by half of his side activate the button
        yMin = 0;
        yMax = transform.localPosition.y;
    }

    private void OnDestroy()
    {
        hoverEntered.RemoveListener(StartPress);
        hoverExited.RemoveListener(EndPress);
    }

    private void StartPress(HoverEnterEventArgs args)
    {
        interactor = (XRBaseInteractor)args.interactorObject;
        previousHandHeight = GetLocalYPosition(interactor.transform.position);
    }

    private void EndPress(HoverExitEventArgs args)
    {
        interactor = null;
        previousHandHeight = 0.0f;

        previousPress = false;
        SetYPosition(yMax);
    }

    //For vertical & horizontal
    //Taking position and converting to localSpace
    private float GetLocalYPosition(Vector3 position)
    {
        Vector3 localPosition = transform.root.InverseTransformPoint(position);
        return localPosition.y;
    }

    private void SetYPosition(float position)
    {
        Vector3 newPosition = transform.localPosition;
        newPosition.y = Mathf.Clamp(position, yMin, yMax);
        transform.localPosition = newPosition;
    }

    private void CheckPress()
    {
        bool inPosition = InPosition();
        if (actualTime > timeToPress)
        {
            if (inPosition && (inPosition = !previousPress))
            {
                OnPress.Invoke();
                actualTime = 0;
            }
        }

        previousPress = inPosition;
    }

    private bool InPosition()
    {
        float inRange = Mathf.Clamp(transform.localPosition.y, yMin, (yMax - yMin) / 4.0f);
        return transform.localPosition.y == inRange;
    }
}
