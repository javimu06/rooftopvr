using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;
public class XRInputInteractionExtendedButtons : MonoBehaviour
{

    //INPUT
    public InputActionProperty ButtonX;
    public InputActionProperty ButtonY;
    public InputActionProperty ButtonA;
    public InputActionProperty ButtonB;

    public UnityEvent PrimaryButtonEvent;
    public UnityEvent SecondaryButtonEvent;

    //INTERACTOR // INTERACTABLE
    XRGrabInteractable grabbable;
    bool selected = false;

    // Start is called before the first frame update
    void Start()
    {
        //Interactable
        grabbable = GetComponent<XRGrabInteractable>();
    }

    // Update is called once per frame
    void Update()
    {
        //INPUT CONTROLLERS
        if (grabbable.GetOldestInteractorSelecting() != null)
        {
            if (grabbable.GetOldestInteractorSelecting().transform.tag == "Left Hand")
            {
                float buttonXpressed = ButtonX.action.ReadValue<float>();
                float buttonYpressed = ButtonY.action.ReadValue<float>();

                if ((buttonXpressed > 0 || buttonYpressed > 0) && !selected)
                {
                    if (buttonXpressed > 0)
                        PrimaryButtonEvent.Invoke();
                    if (buttonYpressed > 0)
                        SecondaryButtonEvent.Invoke();
                    selected = true;
                }
                else if (buttonXpressed == 0 && buttonYpressed == 0)
                {
                    selected = false;
                }
            }
            else if (grabbable.GetOldestInteractorSelecting().transform.tag == "Right Hand")
            {
                float buttonApressed = ButtonA.action.ReadValue<float>();
                float buttonBpressed = ButtonB.action.ReadValue<float>();

                if ((buttonApressed > 0 || buttonBpressed > 0) && !selected)
                {
                    if (buttonApressed > 0)
                        PrimaryButtonEvent.Invoke();
                    if (buttonBpressed > 0)
                        SecondaryButtonEvent.Invoke();
                    selected = true;
                }
                else if (buttonApressed == 0 && buttonBpressed == 0)
                {
                    selected = false;
                }
            }
        }

    }
}
