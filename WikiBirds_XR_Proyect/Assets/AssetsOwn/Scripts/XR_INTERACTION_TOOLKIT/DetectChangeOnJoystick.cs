using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class DetectChangeOnJoystick : MonoBehaviour
{
    [SerializeField]
    InputActionProperty m_RightHandMoveAction;

    [SerializeField]
    UnityEvent evento;
    [SerializeField]
    float stepSoundSize = 0.25f;
    float time = 0;

    private void Update()
    {
        if (JoystickIsActive() && time > stepSoundSize)
        {
            evento.Invoke();
            time = 0;
        }


        time += Time.deltaTime;
    }

    public bool JoystickIsActive()
    {
        if (m_RightHandMoveAction.action?.ReadValue<Vector2>() != Vector2.zero)
            return true;
        else
            return false;
    }

}
