using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPopupInfo : MonoBehaviour
{
    [SerializeField] GameObject popupObject;
    [SerializeField] Transform lookAtObject;

    public bool showPopup;
    private bool isShowing;
    private void Start()
    {
        showPopup = true;
        isShowing = false;
        popupObject.SetActive(isShowing);
    }
    private void Update()
    {
        if (isShowing)
        {
            popupObject.transform.LookAt(new Vector3(lookAtObject.position.x, lookAtObject.transform.position.y, lookAtObject.position.z));
            popupObject.transform.forward *= -1;
        }
    }
    public void activatePopup()
    {
        isShowing = true;
        popupObject.SetActive(isShowing);
    }
    public void deactivatePopup()
    {
        isShowing = false;
        popupObject.SetActive(isShowing);
    }
}
