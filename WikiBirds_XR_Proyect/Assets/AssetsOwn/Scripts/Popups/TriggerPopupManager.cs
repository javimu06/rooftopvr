using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPopupManager : MonoBehaviour
{
    [SerializeField] List<String> popupTags;
    bool showPopups;

    //Objects with the Tags in popupTags
    TriggerPopupInfo triggerPopupInfo;

    private void Start()
    {
        showPopups = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (showPopups && popupTags.Contains(other.gameObject.tag))
        {
            if (other.GetComponent<TriggerPopupInfo>() != null)
            {
                triggerPopupInfo = other.GetComponent<TriggerPopupInfo>();
                triggerPopupInfo.activatePopup();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (showPopups && popupTags.Contains(other.tag))
        {
            if (other.GetComponent<TriggerPopupInfo>() != null)
            {
                triggerPopupInfo = other.GetComponent<TriggerPopupInfo>();
                triggerPopupInfo.deactivatePopup();
            }
        }
    }
}
