using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateGravity : MonoBehaviour
{
    [SerializeField] float recieveCallTimer;
    private float time = 0;
    private void Update()
    {
        time += Time.deltaTime;
    }
    public void activateGravityToObject()
    {
        if (time > recieveCallTimer)
        {
            time = 0;
            if (GetComponent<Rigidbody>() != null)
            {
                GetComponent<Rigidbody>().isKinematic = false;
                GetComponent<Rigidbody>().useGravity = true;
            }
            else
                Debug.Log("RigidBody null");
        }

    }

    public void deactivateGravityToObject()
    {
        if (time > recieveCallTimer)
        {
            time = 0;
            if (GetComponent<Rigidbody>() != null)
            {
                GetComponent<Rigidbody>().isKinematic = true;
                GetComponent<Rigidbody>().useGravity = false;
            }
            else
                Debug.Log("RigidBody null");
        }
    }

    public void switchGravity()
    {
        if (GetComponent<Rigidbody>() != null)
        {
            if (GetComponent<Rigidbody>().useGravity == true)
                deactivateGravityToObject();
            else
                activateGravityToObject();
        }
        else
            Debug.Log("RigidBody null");
    }
}
