using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterial : MonoBehaviour
{
    [SerializeField] Material material_;
    Material original;
    private void Start()
    {
        original = GetComponent<Renderer>().material;
    }
    public void setMaterial()
    {
        GetComponent<Renderer>().material = material_;
    }

    public void resetMaterial()
    {
        GetComponent<Renderer>().material = original;
    }
}
