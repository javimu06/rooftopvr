using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class autoDestruction : MonoBehaviour
{
    [SerializeField]
    float tiempo;

    float timer = 0;

    bool activate = false;
    private void Update()
    {
        if (activate)
        {
            timer += Time.deltaTime;

            if (timer >= tiempo) Destroy(gameObject);
        }

    }

    public void itsTime()
    {
        activate = true;
    }

}
