using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillSystem : MonoBehaviour
{
    public GameObject waterFillDrop;
    public GameObject triggerObject;
    public GameObject ParticleSystemObject;
    public GameObject waterLevel;
    public float spawnDropEveryXSeconds;

    bool instantiateDrop = true;

    // Update is called once per frame
    void Update()
    {
        if (triggerObject.transform.position.y < waterLevel.transform.position.y)
        {
            //Activate Particle Sistem
            triggerObject.SetActive(true);
            ParticleSystemObject.SetActive(true);

            //Instantiate waterFillDrop
            if (instantiateDrop)
            {
                StartCoroutine(spawnFillDrop());
                instantiateDrop = false;
            }
        }
        else
        {
            triggerObject.SetActive(false);
            ParticleSystemObject.SetActive(false);
        }

        //Rotate Trigger
        triggerObject.transform.rotation = Quaternion.identity;
    }

    public IEnumerator spawnFillDrop()
    {
        yield return new WaitForSeconds(spawnDropEveryXSeconds);
        GameObject drop = Instantiate(waterFillDrop, triggerObject.transform);
        StartCoroutine(deleteFillDrop(drop));
        instantiateDrop = true;
    }

    public IEnumerator deleteFillDrop(GameObject drop)
    {
        yield return new WaitForSeconds(2);
        Destroy(drop);
    }

}
