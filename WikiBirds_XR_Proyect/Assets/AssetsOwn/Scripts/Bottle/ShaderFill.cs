using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderFill : MonoBehaviour
{
    Material mt;
    float amount = 0;
    float amountToFill = 0;
    public float fillAmountPerDrop;
    int fillID;
    void Start()
    {
        mt = GetComponent<MeshRenderer>().material;
        //Guardamos el ID del Fill para no llamarlo todo el rato
        fillID = Shader.PropertyToID("_Fill");
        mt.SetFloat(fillID, amount);
    }

    void Update()
    {
        if (amountToFill > 0.01f)
        {
            float amountFillPerFrame = amountToFill * Time.deltaTime;
            amount += amountFillPerFrame;
            mt.SetFloat(fillID, amount);
            amountToFill -= amountFillPerFrame;
        }
    }
    public void fillBottle()
    {
        amountToFill += fillAmountPerDrop;
        if (amountToFill >= 1)
            amountToFill = 1;
    }
}
