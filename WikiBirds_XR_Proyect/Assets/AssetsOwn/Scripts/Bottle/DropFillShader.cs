using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropFillShader : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        ShaderFill glassFill = other.GetComponent<ShaderFill>();
        if (glassFill != null)
        {
            glassFill.fillBottle();
        }
    }
}
