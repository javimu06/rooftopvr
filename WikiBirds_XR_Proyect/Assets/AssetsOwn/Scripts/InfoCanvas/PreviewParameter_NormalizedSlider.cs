using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PreviewParameter_NormalizedSlider : MonoBehaviour
{
    [Header("Slidder de Valores Ideales")]
    [SerializeField] GameObject idealValuesSlider;

    [Header("Slidder de Valores Actuales")]
    [SerializeField] GameObject actualValuesSlider;
    float actualValue;

    [Header("Planta Gameobject")]
    [SerializeField] GameObject plantGameobject;
    Planta planta;

    [Header("Canvas Text")]
    [SerializeField] TextMeshProUGUI textoCanvas;

    enum plantaParam { Agua, Humedad, Temperatura };
    [SerializeField] plantaParam parametro;

    [Header("Rango de valores ideales")]
    [SerializeField] float minRang = 20;
    [SerializeField] float maxRang = 40;
    [Header("Mayor diferencia respecto a los valores ideales")]
    [SerializeField] float maxDiff = 25;

    //Diferencia entre minRang y maxRang
    float diffRang; //20
    //Primer y ultimo valor del total del slider, para posteriormente normalizarlo [0,1]
    //minValor = (minRang - maxDiff), maxValor = (maxRang + maxDiff)
    float minValor; //-5
    float maxValor; //65
    //Diferencia entre minValor y maxValor
    float diffMinMaxValor;  //70
    //Valor de la normalizacion
    float scaleValue;       //0,0142857142857143

    private void Start()
    {
        planta = plantGameobject.GetComponent<Planta>();
        if (planta != null)
        {
            if (parametro == plantaParam.Agua)
            {
                minRang = planta.rangoAgua[0];
                maxRang = planta.rangoAgua[planta.rangoAgua.Length - 1];
                maxDiff = planta.diferenciasAgua[planta.diferenciasAgua.Length - 1];
            }
            else if (parametro == plantaParam.Humedad)
            {
                minRang = planta.rangoHumedad[0];
                maxRang = planta.rangoHumedad[planta.rangoHumedad.Length - 1];
                maxDiff = planta.diferenciasHumedad[planta.diferenciasHumedad.Length - 1];
            }
            else if (parametro == plantaParam.Temperatura)
            {
                minRang = planta.rangoTemperatura[0];
                maxRang = planta.rangoTemperatura[planta.rangoTemperatura.Length - 1];
                maxDiff = planta.diferenciasTemperatura[planta.diferenciasTemperatura.Length - 1];
            }
            if (textoCanvas != null)
            {
                if (parametro == plantaParam.Agua)
                    textoCanvas.text = "Agua";
                else if (parametro == plantaParam.Humedad)
                    textoCanvas.text = "Humedad";
                else if (parametro == plantaParam.Temperatura)
                    textoCanvas.text = "Temperatura";
            }
        }

        diffRang = maxRang - minRang;

        minValor = (minRang - maxDiff);
        maxValor = (maxRang + maxDiff);

        diffMinMaxValor = maxValor - minValor;

        scaleValue = 1 / diffMinMaxValor;

    }
    private void Update()
    {
        ActualizaValores();
    }
    void ActualizaValores()
    {
        idealValuesSlider.transform.localPosition = new Vector3(maxDiff * scaleValue, 0, 0);
        idealValuesSlider.transform.localScale = new Vector3(diffRang * scaleValue * 10, 1, 1);

        if (planta != null)
        {
            if (parametro == plantaParam.Agua)
            {
                actualValue = planta.get_aguaEnTierra();
            }
            else if (parametro == plantaParam.Humedad)
            {
                actualValue = planta.get_humedad_Actual();
            }
            else if (parametro == plantaParam.Temperatura)
            {
                actualValue = planta.get_temperatura_Actual();
            }
        }

        //Si el valor esta dentro de los intervalos extremos
        if (actualValue > minValor && actualValue < maxValor)
        {
            actualValuesSlider.transform.localPosition = new Vector3(actualValue * scaleValue, 0, 0);
            actualValuesSlider.transform.localScale = new Vector3(scaleValue * 10, 1, 1);
        }
    }
}
