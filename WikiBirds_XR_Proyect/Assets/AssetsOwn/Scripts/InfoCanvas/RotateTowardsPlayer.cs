using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTowardsPlayer : MonoBehaviour
{
    [SerializeField] Transform lookAtObject;

    // Update is called once per frame
    void Update()
    {
        this.transform.LookAt(new Vector3(lookAtObject.position.x, lookAtObject.transform.position.y, lookAtObject.position.z));
        this.transform.forward *= -1;
    }
}
