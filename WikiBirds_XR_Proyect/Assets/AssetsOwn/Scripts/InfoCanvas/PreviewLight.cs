using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PreviewLight : MonoBehaviour
{
    [Header("Planta Gameobject")]
    [SerializeField] GameObject plantGameobject;
    Planta planta;

    [Header("Canvas Text")]
    [SerializeField] TextMeshProUGUI textoCanvas;

    enum plantaParam { Luz, Plaga };
    [SerializeField] plantaParam parametro;

    [Header("SpawnPoint")]
    [SerializeField] List<GameObject> previewObjects;

    private void Start()
    {
        planta = plantGameobject.GetComponent<Planta>();

        if (parametro == plantaParam.Luz)
            textoCanvas.text = "Luz Actual";
        else if (parametro == plantaParam.Plaga)
            textoCanvas.text = "Plaga Actual";
    }
    private void Update()
    {
        actualizaParametro();
    }
    void actualizaParametro()
    {
        if (planta != null)
        {
            if (parametro == plantaParam.Luz)
            {

                if (planta.tipoDeLuzRecibidaPorShandokan == Luz.Directa)
                {
                    previewObjects[0].SetActive(true);
                    previewObjects[1].SetActive(false);
                    previewObjects[2].SetActive(false);
                }
                else if (planta.tipoDeLuzRecibidaPorShandokan == Luz.Indirecta)
                {
                    previewObjects[0].SetActive(false);
                    previewObjects[1].SetActive(true);
                    previewObjects[2].SetActive(false);
                }
                else if (planta.tipoDeLuzRecibidaPorShandokan == Luz.Penumbra)
                {
                    previewObjects[0].SetActive(false);
                    previewObjects[1].SetActive(false);
                    previewObjects[2].SetActive(true);
                }
            }
            else if (parametro == plantaParam.Plaga)
            {
                if (planta.plaga == Plagas.Aranias)
                {
                    previewObjects[0].SetActive(true);
                    previewObjects[1].SetActive(false);
                    previewObjects[2].SetActive(false);
                    previewObjects[3].SetActive(false);
                }
                else if (planta.plaga == Plagas.Gusanos)
                {
                    previewObjects[0].SetActive(false);
                    previewObjects[1].SetActive(true);
                    previewObjects[2].SetActive(false);
                    previewObjects[3].SetActive(false);
                }
                else if (planta.plaga == Plagas.Moscas)
                {
                    previewObjects[0].SetActive(false);
                    previewObjects[1].SetActive(false);
                    previewObjects[2].SetActive(true);
                    previewObjects[3].SetActive(false);
                }
                else if (planta.plaga == Plagas.Ninguna)
                {
                    previewObjects[0].SetActive(false);
                    previewObjects[1].SetActive(false);
                    previewObjects[2].SetActive(false);
                    previewObjects[3].SetActive(true);
                }
            }
        }
        else
            Debug.Log("Planta Missing");
    }
}
