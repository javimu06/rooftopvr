using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class notaSpawner : MonoBehaviour
{
    public Transform[] spawns;
    bool[] spawnsOcuped = new bool[4];
    public Transform centro;
    public GameObject nota;
    private GameObject newInfoCanvasOliMoli;
    estado actual;

    private void Start()
    {
        newInfoCanvasOliMoli = GameManager.instance.newInfoCanvas;

    }
    //Mismo orden que las imagenes
    //Orden: primero agua, luego temp, luego hum, luego luz, luego plagas (de menor a mayor)
    public bool[] aparecidas = new bool[20];

    public void finDeDia()
    {
        Planta p = GetComponent<Planta>();

        actual = p.notas();

        if (actual != estado.ninguno && !aparecidas[(int)actual - 1])
        {
            spawnNote();
            aparecidas[(int)actual - 1] = true;
        }


    }

    public void encuadernar()
    {
        libretaManager libreta = GameManager.instance.getLibreta().GetComponent<libretaManager>();
        if (newInfoCanvasOliMoli != null)
            newInfoCanvasOliMoli.GetComponent<CanvasImageFader>().startFadeIn();

        Planta p = GetComponent<Planta>();

        int tipoInfo;
        int caso = (int)actual;

        tipoInfo = chechParametro(caso);

        libreta.enableInfo(p.getTipo(), tipoInfo, p.GetPlagas());
    }

    private static int chechParametro(int caso)
    {
        int tipoInfo;
        if (caso <= 6) tipoInfo = 0;
        else if (caso <= 12) tipoInfo = 1;
        else if (caso <= 18) tipoInfo = 2;
        else tipoInfo = 3;
        return tipoInfo;
    }

    private void spawnNote()
    {
        bool allOcuped = true;
        for (int i = 0; i < spawns.Length && allOcuped; ++i)
        {
            if (!spawnsOcuped[i])
                allOcuped = false;
        }
        if (!allOcuped)
        {
            int pos = Random.Range(0, spawns.Length);
            do
            {
                pos = Random.Range(0, spawns.Length);
            }
            while (spawnsOcuped[pos]);

            spawnsOcuped[pos] = true;
            Vector3 dir = -centro.position + spawns[pos].position;
            Quaternion rot = Quaternion.LookRotation(dir);

            GameObject note = Instantiate(nota, spawns[pos].position, rot, spawns[pos]);
            note.GetComponent<notasController>().notaSpawner_ = GetComponent<notaSpawner>();
            note.GetComponent<notasController>().setInfo(chechParametro((int)actual));
        }
    }
}
