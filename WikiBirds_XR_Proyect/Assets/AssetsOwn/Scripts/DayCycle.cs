using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;

public class DayCycle : MonoBehaviour
{
    [SerializeField] Light light_;
    private Quaternion originalRotation;
    [SerializeField] Material SkyboxMaterialDay;
    [SerializeField] Material SkyboxMaterialNight;

    [SerializeField] float stepRotation;
    [SerializeField] float stepSpeed;
    float actualTime;
    float actualRotation;

    [SerializeField] float maxIntensity = 4;
    [SerializeField] float minIntensity = 0;
    [SerializeField] float minIntensityCAPED = 1;
    private float stepIntensity;

    [SerializeField] float maxfogDensity = 0.1f;
    [SerializeField] float minfogDensity = 0.01f;
    float fogDensityStep;

    [SerializeField] float maxfogColor;
    [SerializeField] float minfogColor = 0;
    float fogColorStep;

    [SerializeField] UnityEvent playCrickets;
    [SerializeField] UnityEvent stopCrickets;
    bool lastState = true;  //True == Day

    // Start is called before the first frame update
    void Start()
    {
        originalRotation = light_.transform.rotation;
        maxfogColor = RenderSettings.fogColor.r;
        stepIntensity = (maxIntensity - minIntensity) / (180 / stepRotation);
        fogDensityStep = (maxfogDensity - minfogDensity) / (180 / stepRotation);
        fogColorStep = (maxfogColor - minfogColor) / (180 / stepRotation);

        actualTime = 0;
        setupDay();
    }

    // Update is called once per frame
    void Update()
    {
        actualTime += Time.deltaTime;
        if (actualTime > stepSpeed)
        {
            actualTime = 0;
            actualRotation += stepRotation;

            //if (variable != null)
            //{
            //    variable.setParameterValue(actualRotation % 360);
            //}

            light_.transform.Rotate(stepRotation, 0, 0);

            if (actualRotation % 360 > 270 || actualRotation % 360 < 90)
            {
                //Se va haciendo de dia
                light_.intensity += stepIntensity;
                RenderSettings.fogDensity -= fogDensityStep;
                RenderSettings.fogColor += new Color(fogColorStep, fogColorStep, fogColorStep);
            }
            else //if (actualRotation % 360 > 90)
            {
                //Se va haciendo de noche
                if (light_.intensity != minIntensity)
                    light_.intensity -= stepIntensity;
                if (light_.intensity < minIntensityCAPED)
                    light_.intensity = minIntensity;
                RenderSettings.fogDensity += fogDensityStep;
                RenderSettings.fogColor -= new Color(fogColorStep, fogColorStep, fogColorStep);
            }

        }

        if (actualRotation % 360 == 180)
        {
            Debug.Log("Make Night");
            if (lastState)
                makeNight();
        }

        if (actualRotation % 360 == 0)
        {
            Debug.Log("Make Day");
            if (!lastState)
                makeDay();
        }
    }

    void makeNight()
    {
        RenderSettings.skybox = SkyboxMaterialNight;
        lastState = false;
        playCrickets.Invoke();
    }

    void makeDay()
    {
        RenderSettings.skybox = SkyboxMaterialDay;
        lastState = true;
        stopCrickets.Invoke();
    }

    public void setupDay()
    {
        Debug.Log("Setup day");
        makeDay();
        actualRotation = 90;
        light_.intensity = maxIntensity;
        RenderSettings.fogDensity = minfogDensity;
        RenderSettings.fogColor = new Color(maxfogColor, maxfogColor, maxfogColor);
        light_.transform.rotation = originalRotation;

    }

    public bool isDay()
    {
        return lastState;
    }
}
