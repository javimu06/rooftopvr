using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class libretaManager : MonoBehaviour
{
    public enum parametro { agua, tempertatura, humedad, luz, plagas }

    //Cada una tiene 4
    //Agua, temperatura, humedad, luz
    [SerializeField]
    GameObject[] monsteraInfo;
    [SerializeField]
    GameObject[] violetaInfo;
    [SerializeField]
    GameObject[] peyoteInfo;
    [SerializeField]
    GameObject[] coronaInfo;
    [SerializeField]
    GameObject[] anturioInfo;

    public void enableInfo(tipo planta, int param, Plagas plaga)
    {
        switch (planta)
        {
            case tipo.Monstera:
                monsteraInfo[param].SetActive(true);
                break;
            case tipo.Violeta:
                peyoteInfo[param].SetActive(true);
                break;
            case tipo.Peyote:
                peyoteInfo[param].SetActive(true);
                break;
            case tipo.Corona:
                peyoteInfo[param].SetActive(true);
                break;
            case tipo.Anturio:
                peyoteInfo[param].SetActive(true);
                break;
            default:
                break;
        }
    }
}
