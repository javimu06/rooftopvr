using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollide : MonoBehaviour
{
    public int noPlayerCollisionLayer;
    private bool collisionEnabled = true;

    public void disablePlayerCollision()
    {
        collisionEnabled = false;
        gameObject.layer = noPlayerCollisionLayer;
    }

    public void enablePlayerCollision()
    {
        collisionEnabled = true;
        gameObject.layer = 0;
    }

    public void switchPlayerCollision()
    {
        if (collisionEnabled)
            disablePlayerCollision();
        else
            enablePlayerCollision();

        collisionEnabled = !collisionEnabled;
    }

    public bool isPlayerCollision()
    {
        return collisionEnabled;
    }
}
