using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

public class BookManager : MonoBehaviour
{
    public Canvas leftCanvas;
    GameObject childLeft;
    public Canvas rightCanvas;
    GameObject childRight;

    int actualPage;

    //INPUT
    //public InputActionProperty ButtonX;
    //public InputActionProperty ButtonY;
    //public InputActionProperty ButtonA;
    //public InputActionProperty ButtonB;

    //INTERACTOR // INTERACTABLE
    //XRGrabInteractable grabbable;
    //bool selected = false;

    // Start is called before the first frame update
    void Start()
    {
        //First Page Set
        actualPage = 0;
        childLeft = leftCanvas.transform.GetChild(actualPage).gameObject;
        childRight = rightCanvas.transform.GetChild(actualPage).gameObject;
        if (childLeft != null && childRight != null)
        {
            childLeft.SetActive(true);
            childRight.SetActive(true);
        }

        //Interactable
        //grabbable = GetComponent<XRGrabInteractable>();
    }

    public void nextPages()
    {
        if (actualPage < leftCanvas.transform.childCount - 1)
        {
            childLeft.SetActive(false);
            childRight.SetActive(false);

            actualPage++;
            childLeft = leftCanvas.transform.GetChild(actualPage).gameObject;
            childRight = rightCanvas.transform.GetChild(actualPage).gameObject;

            childLeft.SetActive(true);
            childRight.SetActive(true);
        }
    }

    public void backPages()
    {
        if (actualPage > 0)
        {
            childLeft.SetActive(false);
            childRight.SetActive(false);

            actualPage--;
            childLeft = leftCanvas.transform.GetChild(actualPage).gameObject;
            childRight = rightCanvas.transform.GetChild(actualPage).gameObject;
            childLeft.SetActive(true);
            childRight.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //INPUT ON KEYBOARD
        if (Input.GetKeyDown(KeyCode.D))
        {
            //Siguientes paginas
            nextPages();
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            //Anteriores paginas
            backPages();
        }

    }
}
