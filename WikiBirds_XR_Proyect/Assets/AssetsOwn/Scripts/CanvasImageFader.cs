using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasImageFader : MonoBehaviour
{
    [SerializeField] Image canvasImage;
    [SerializeField] float fadeInTime;
    [SerializeField] float fadeOutTime;
    [SerializeField] float waitTime;
    [SerializeField] bool automaticFadeOut;

    private float currentTime;
    private bool fadeIn;
    private bool fadeOut;
    private bool waiting;

    public void startFadeIn() { fadeIn = true; }
    public void startFadeOut() { fadeOut = true; }
    // Start is called before the first frame update
    void Start()
    {
        currentTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (fadeIn)
        {
            if (currentTime < fadeInTime)
            {
                var fadeInStep = currentTime / fadeInTime;
                currentTime += Time.deltaTime;

                float intToByte = fadeInStep * 260;
                if (intToByte <= 255 && intToByte >= 0)
                {
                    byte aux = System.Convert.ToByte(intToByte);
                    canvasImage.color = new Color32(255, 255, 225, aux);
                }
            }
            else
            {
                fadeIn = false;
                waiting = true;
                canvasImage.color = new Color32(255, 255, 225, 255);
                currentTime = 0;
            }
        }
        if (automaticFadeOut)
        {
            if (waiting && !fadeIn && !fadeOut)
            {
                if (currentTime < waitTime)
                {
                    currentTime += Time.deltaTime;
                }
                else
                {
                    fadeOut = true;
                    waiting = false;
                    currentTime = 0;
                }
            }
        }
        if (fadeOut)
        {
            if (currentTime < fadeOutTime)
            {
                var fadeOutStep = 1 - (currentTime / fadeOutTime);
                currentTime += Time.deltaTime;

                float intToByte = fadeOutStep * 260;
                if (intToByte >= 0 && intToByte <= 255)
                {
                    byte aux = System.Convert.ToByte(intToByte);
                    canvasImage.color = new Color32(255, 255, 225, aux);
                }
            }
            else
            {
                fadeOut = false;
                currentTime = 0;
                canvasImage.color = new Color32(255, 255, 225, 0);
            }
        }
    }
}
