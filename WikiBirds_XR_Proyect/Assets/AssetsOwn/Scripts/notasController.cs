using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class notasController : MonoBehaviour
{
    //Orden: primero agua, luego temp, luego hum, luego luz, luego plagas (de menor a mayor)
    [SerializeField]
    Sprite[] imagenes;
    [SerializeField] Image image_;
    public notaSpawner notaSpawner_;

    public void setInfo(int index)
    {
        image_.sprite = imagenes[index];
    }

    public void deleteNote()
    {
        Destroy(this.gameObject, 5);
    }

    public void notaSpawnerEncuadernar()
    {
        notaSpawner_.encuadernar();
    }
}
