using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectToPositionManager : MonoBehaviour
{
    public GameObject[] objetos;
    [SerializeField] private Transform[] posiciones;

    public void posicionaObjetos()
    {
        for (int i = 0; i < objetos.Length; ++i)
        {
            objetos[i].transform.position = posiciones[i].position;
            objetos[i].transform.rotation = posiciones[i].rotation;
        }
    }


}
