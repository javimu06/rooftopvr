using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pochas : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    Transform[] posiciones_disponibles;

    [SerializeField]
    Transform centro;

    [SerializeField]
    GameObject pocha;

    int prob = 0;

    [SerializeField]
    int increment = 0;

    private void aparecen()
    {
        for (int i = 0; i < posiciones_disponibles.Length; i++)
        {
            // Calculos para generar una planta
            Vector3 dir = -centro.position + posiciones_disponibles[i].position;

            // Generar el prefab
            GameObject nueva_pocha = Instantiate(pocha, posiciones_disponibles[i]);
            //nueva_pocha.transform.SetParent(posiciones_disponibles[i]);

            int aux = Random.Range(7, 15);
            float factor_escala = aux * 0.1f;
            nueva_pocha.transform.localScale *= factor_escala;

            Quaternion rot = Quaternion.LookRotation(dir);
            nueva_pocha.transform.rotation = rot;
        }
    }

    public void finDia()
    {
        prob += increment;
        int random = Random.Range(0, 101);

        if (random < prob)
        {
            aparecen();
            prob = 0;
        }

    }
}
