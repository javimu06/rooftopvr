using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitController : MonoBehaviour
{
    public Transform objectA;
    public Transform objectB;
    public Transform objectC;
    public float minDistance = 1f;
    public float orbitSpeed = 10f;
    public float moveSpeed = 5f;

    private Vector3 orbitAxis;

    private void Start()
    {
        // Calculate the initial orbit axis based on the relative positions of Object A and Object B
        orbitAxis = Vector3.Cross(objectA.position - objectB.position, Vector3.up).normalized;
    }

    private void Update()
    {
        // Calculate the distance between Object A and Object C
        float distance = Vector3.Distance(objectA.position, objectC.position);

        // Calculate the target position for Object A
        Vector3 targetPosition = objectB.position + orbitAxis * minDistance;

        // Move Object B towards Object C
        objectB.position = Vector3.MoveTowards(objectB.position, objectC.position, moveSpeed * Time.deltaTime);

        // Rotate Object A around Object B if the distance to Object C is greater than the minimum distance
        if (distance > minDistance)
        {
            objectA.RotateAround(objectB.position, orbitAxis, orbitSpeed * Time.deltaTime);
        }
        else
        {
            // Calculate a new orbit axis based on the current positions of Object A and Object B
            orbitAxis = Vector3.Cross(objectA.position - objectB.position, Vector3.up).normalized;
        }

        // Set the position of Object A to the target position
        objectA.position = targetPosition;
    }
}
