using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GardenerCheats : MonoBehaviour
{
    [SerializeField] GameObject child;
    bool show = false;

    public void ShowStats()
    {

        if (GameManager.instance.GetCheats())
        {
            show = true;
            child.SetActive(show);
        }
    }

    public void DontShowStats()
    {
        show = false;
        child.SetActive(show);
    }

    public void ToggleStats()
    {
        if (GameManager.instance.GetCheats())
        {
            show = !show;
            child.SetActive(show);
        }
    }

}
